const axios = require('axios');
const test_data = require('../constants/test_data');
const translate_uri = 'https://translate.yandex.net';
const api_key = 'trnsl.1.1.20190128T190632Z.66806afd25fcad1b.aa2561c1eb7a91588e9cfae52cadf0550686518f';
var translated_text;
var response_translate;


function get_translate_url(site) {
    return site + '/api/v1.5/tr.json/translate'
}

async function get_translate(text) {
    return (await axios.get(get_translate_url(translate_uri), {
        params: {
            key: api_key,
            text: text,
            lang: 'en-ru',
            format: 'plain'
        }
    }));
}

beforeAll(() => {
    return get_translate('translate')
        .then(response => {
            response_translate = response;
            translated_text = response.data.text;
        });
});


test('two plus two is four', () => {
    expect(2 + 2).toBe(4);
});

test('should be normal ans', function () {
    expect(response_translate.status).toBe(200);
    expect(translated_text).toContain('перевести');
});

test('should be abnormal ans', function () {
    expect(response_translate.status).toBe(200);
    expect(translated_text).not.toContain('butter');
});


test.each(test_data)('%# test %o', function (obj) {
    return get_translate(obj.text).then(response => {
        expect(response.status).toBe(200);
        expect(response.data.text).toContain(obj.expected);
        console.log(obj);
        console.log(response);
    });
});